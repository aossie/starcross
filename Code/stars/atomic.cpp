#include "stars/atomic.h"
#include "stars/lay.h"
#include <vector>
#include <map>
#include <string>

using namespace std;

namespace {
	
	struct Atoms {
		Atoms() {
				}
		vector<char *> table;
		map<string, int> lookup;
	};
	
	Atoms * atoms = NULL;

	void InitAtoms();
	
	int GetAtomIndex( const char * str ) {
		InitAtoms();
		string s( str );
		if ( atoms->lookup.count( s ) ) {
			return atoms->lookup[ s ];
		}
		int v = atoms->table.size();
		atoms->table.push_back( strdup( str ) );
		
		atoms->lookup[ s ] = v;
		return v;
	}
	
	char * GetAtomString( const char * str) {
		InitAtoms();
		string s( str );
		if ( atoms->lookup.count( s ) ) {
			int v = atoms->lookup[ s ];
			return atoms->table[ v ];
		}
		return NULL;
	}
	
	void InitAtoms() {
		if ( atoms == NULL ) {
			atoms = new Atoms;
		}		
	}
	

}	

namespace stars {


	atomic Findatomic( const char *str ) {
		Initatomics();
		string s( str );
		if ( atomics->lookup.count( s ) ) {
			return atomic( str );
		}
		return atomic();
	}
	
	int GetatomicTableSize() {
		return atomics->table.size();
	}

	atomic Getatomic( int i ) {
		if ( i < (int)atomics->table.size() ) {
			return Findatomic( atomics->table[ i ] );
		}
		return atomic();
	}

	
	atomic::atomic( const char * str ) 
	: v( GetatomicIndex( str ) )
	, s( GetatomicString( str ) )
	{
	}
	

}
