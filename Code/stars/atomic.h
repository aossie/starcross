#ifndef __STARS_ATOMIC_H__
#define __STARS_ATOMIC_H__
#include <string>
#include <vector>

namespace stars {

	struct Atomic {
	private:
		int v;
		char *s;

	public:
		
		Atomic() : v( -1 ), s( 0 ) {
		}
		Atomic( const atomic & a ) : v( a.v ), s( a.s ) {
		}
		
		Atomic( const char *str );
		
		unsigned int Val() const {
			return v;
		}

		std::string Str() const {
			return s;
		}
		
		bool Valid() const {
			return v >= 0;
		}
				
		bool operator == ( const Atomic & rhs ) const {
			return v == rhs.v;
		}
		
		bool operator != ( const Atomic & rhs ) const {
			return v != rhs.v;
		}
		
		bool operator < ( const Atomic & rhs ) const {
			return v < rhs.v;
		}
		
	};
	
	// Will return an invalid atomic if the string is not already in the table.
	Atomic FindAtomic( const char * str );
	
	// Get info about the atomic table
	int GetAtomicTableSize();
	Atomic GetAtomic( int i );

}

#endif // __ATOMIC_H__
