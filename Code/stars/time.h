#ifndef __STARS_TIME_H__
#define __STARS_TIME_H__

#if __APPLE__
# include <sys/time.h>
#include <unistd.h>
#elif _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <sys/timeb.h>
#include <time.h>
#endif

namespace stars {

#if __APPLE__
	inline double GetTime() {
		timeval tv;
		gettimeofday( &tv, NULL );
		return tv.tv_sec + tv.tv_usec / 1000000.0;
	}
# define Gmtime gmtime

#endif

#if _WIN32
	inline double GetTime() {
		struct _timeb timebuffer;
		_ftime64_s( &timebuffer );
		return timebuffer.time + timebuffer.millitm / 1000.0;
	}
	inline struct tm *Gmtime( const time_t *gmt ) {
		static tm val;
		gmtime_s( &val, gmt );
		return &val;
	}
#endif

	inline float GetSeconds() {
		static double tstartup;
		double t = GetTime();
		if ( tstartup == 0.0 ) {
			tstartup = t;
		}
		return float( t - tstartup );
	}
	
	inline void SleepMilliseconds( int i ) {
#ifdef __APPLE__
		usleep( i * 1000 );
#elif _WIN32
		Sleep( i );
#endif
	}
	
}

#endif // __STARS_TIME_H__
