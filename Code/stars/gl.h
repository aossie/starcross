#ifndef __STARS_GL_H__
#define __STARS_GL_H__


#ifdef __APPLE__
# include <TargetConditionals.h>
# if TARGET_OS_IPHONE
#  include <OpenGLES/ES1/gl.h>
#  include <OpenGLES/ES1/glext.h>
#  define GL_TEXTURE_1D 0
#  define GL_TEXTURE_3D 0
#  define GL_TEXTURE_CUBE_MAP 0
#  define GL_TEXTURE_LOD_BIAS GL_TEXTURE_LOD_BIAS_EXT
#  define GL_TEXTURE_MIN_LOD 0
#  define GL_TEXTURE_MAX_LOD 0
#  define GL_DEPTH_COMPONENT GL_DEPTH_COMPONENT16_OES
#  define glGenerateMipmapEXT glGenerateMipmapOES
#  define GlInternalFormat GlFormat

#  define GL_FRAMEBUFFER_EXT GL_FRAMEBUFFER_OES
#  define GL_RENDERBUFFER_EXT GL_RENDERBUFFER_OES
#  define GL_COLOR_ATTACHMENT0_EXT GL_COLOR_ATTACHMENT0_OES
#  define glGenRenderbuffersEXT glGenRenderbuffersOES
#  define glDeleteRenderbuffersEXT glDeleteRenderbuffersOES
#  define glBindRenderbufferEXT glBindRenderbufferOES
#  define glRenderbufferStorageEXT glRenderbufferStorageOES
#  define glGenerateMipmapEXT glGenerateMipmapOES

# else
#  include <OpenGL/gl.h>
# include "stars/GL/glext.h"
# endif
#endif

#if _MSC_VER
# include <windows.h>
# include <GL/gl.h>
# include "stars/GL/glext.h"
# include "stars/GL/entry.h"
#endif


#endif // __R3_GL_H__
