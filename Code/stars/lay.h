#ifndef __STARS_LAY_H__
#define __STARS_LAY_H__

#include <string>

#define ARRAY_ELEMENTS( a ) ( sizeof( a ) / sizeof ( a[0] ) )

namespace stars {
	
	typedef long long int64;
	typedef unsigned long long uint64;
	typedef unsigned int uint;
	typedef unsigned short ushort;
	typedef	unsigned char uchar;
	typedef unsigned char byte;
	
}

#if _MSC_VER
# pragma warning(disable: 4018) // signed/unsigned mismatch
# pragma warning(disable: 4244) // conversion possible loss of data
# pragma warning(disable: 4305) // truncation of double to float

# define strdup _strdup
# define starsSscanf( src, fmt, ... ) sscanf_s( src, fmt, ## __VA_ARGS__ )
# define starsSprintf( dst, fmt, ... ) sprintf_s( dst, ARRAY_ELEMENTS( dst ), fmt, ## __VA_ARGS__ )
# define starsVsprintf( dst, fmt, args ) vsprintf_s( dst, ARRAY_ELEMENTS( dst ), fmt, args )
#else
# define starsSscanf  sscanf
# define starsSprintf sprintf
# define starsVsprintf vsprintf
#endif

#define starsToLower tolower

inline std::string LowerCase( const std::string & str ) {
	std::string ret = str;
	for ( int i = 0; i < (int)ret.size(); i++ ) {
		ret[ i ] = r3ToLower( ret[ i ] );
	}
	return ret;
}

#endif // __STARS_LAY_H__
