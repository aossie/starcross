#ifndef __STARS_OUTCOME_H__
#define __STARS_OUTCOME_H__

namespace stars {
	
	void InitOutput();
	
	void Output( const char *fmt, ... );
	void OutputDebug( const char *fmt, ... );
	
}

#endif // __STARS_OUTCOME_H__
