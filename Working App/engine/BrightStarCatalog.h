#ifndef __BRIGHTSTARCATALOG_DEF__
#define __BRIGHTSTARCATALOG_DEF__

#include <vector>

/*
  The 518 brightest stars, from the Yale Bright Star Catalog, BSC5.
  These are all the stars brighter than 4th magnitude.
*/

class BrightStarCatalog 
{
public:
	bool Initialized();
	int GetSize() {
		return (int)stars.size();
	}
  	float rightAscensionInRadians(int i);
  	float declinationInRadians(int i);
  	float mag(int i);
private:
	struct Star {
		float ra;
		float dec;
		float mag;
	};
	std::vector<Star> stars;
};

#endif //__BRIGHTSTARCATALOG_DEF__
