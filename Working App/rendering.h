
#ifndef __STARCROSS_RENDERING_H__
#define __STARCROSS_RENDERING_H__

#include "stars/binding.h"
#include "stars/straight.h"
#include "stars/texture.h"
#include <string>
#include <vector>

namespace starcross {

	extern stars::Vec3f UpVector;
	
	
	// we get a static list of these for each star at
	// startup, and generate a new list for the solar system
	// each frame...
	struct Sprite {
		std::string name;
		r3::Vec3f direction;
		float magnitude;
		float scale;
		stars::Vec4f color;
		stars::Texture2D *tex;
	};
	
	// for constellations
	struct Lines {
		std::string name;
        std::string localizedName;
		std::vector< stars::Vec3f > vert;
		stars::Vec3f center;
		float limit;
	};
			
	void Clear();
	void SetColor( const stars::Vec4f & c );
    
	void PushTransform();
	void ClearTransform();
	void PopTransform();
	
	void ApplyTransform( const stars::Matrix4f &m );
	stars::Matrix4f GetTransform();
	
	void DrawQuad(  float radius, const stars::Vec3f & direction  ); 
	
	void DrawSprite( stars::Texture2D *tex, stars::Bounds2f bounds );
	
    void DrawSprite( stars::Texture2D *tex, float radius, const stars::Vec3f & direction, const stars::Vec3f & lookDir );
    
	void DrawSprite( stars::Texture2D *tex, float radius, const stars::Vec3f & direction ); 

	stars::OrientedBounds2f StringBounds( const std::string & str, const stars::Vec3f & direction );

	void DrawString( const std::string & str, const stars::Vec3f & direction);
	void DrawStringAtLocation( const std::string & str, const stars::Vec3f & position, const stars::Matrix4f & rotation );
	
	void DrawDebugGrid();

	stars::Vec3f SphericalToCartesian( float rho, float phi, float theta );
	stars::Vec3f CartesianToSpherical( const stars::Vec3f & c );
}

#endif //__STARCROSS_RENDERING_H__
