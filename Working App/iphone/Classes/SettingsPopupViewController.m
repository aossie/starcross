

#import "SettingsPopupViewController.h"
#import "MainViewController.h"
#import "star3mapAppDelegate.h"

@implementation SettingsPopupViewController

- (instancetype)init{
    if (self = [super init]) {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        CGSize screenSize = CGSizeMake(screenBounds.size.width, screenBounds.size.height);
        
        self.contentSizeInPopup = CGSizeMake(screenSize.width, 240);
        
        self.popupController.navigationBarHidden = YES;
    }
    return self;
}

- (void)dismiss{

}
                                                  
- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGSize screenSize = CGSizeMake(screenBounds.size.width, screenBounds.size.height);
    
    self.view.backgroundColor = [UIColor colorWithRed:25.0/255.0 green:25.0/255.0 blue:25.0/255.0 alpha:1.0];
    
    _music = [UIButton buttonWithType:UIButtonTypeCustom];
    [_music setFrame:CGRectMake((screenSize.width-44)/2+30, 15, 44, 44)];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"MusicOn"]){
        [_music setImage:[UIImage imageNamed:@"controls_music_on"] forState:UIControlStateNormal];
    } else {
        [_music setImage:[UIImage imageNamed:@"controls_music"] forState:UIControlStateNormal];
    }
    [_music addTarget:self action:@selector(musicPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_music];
    
    
    _camera = [UIButton buttonWithType:UIButtonTypeCustom];
    [_camera setFrame:CGRectMake(_music.frame.origin.x + 59, 15, 44, 44)];
    [_camera setImage:[UIImage imageNamed:@"controls_snapshot"] forState:UIControlStateNormal];
    [_camera addTarget:self action:@selector(cameraPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_camera];
    
    _arMode = [UIButton buttonWithType:UIButtonTypeCustom];
    [_arMode setFrame:CGRectMake(_music.frame.origin.x - 59, 15, 44, 44)];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"ARModeOn"]){
        [_arMode setImage:[UIImage imageNamed:@"controls_ar_camera_on"] forState:UIControlStateNormal];
    } else {
        [_arMode setImage:[UIImage imageNamed:@"controls_ar_camera"] forState:UIControlStateNormal];
    }
    [_arMode addTarget:self action:@selector(arModePressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_arMode];
    
    _nightMode = [UIButton buttonWithType:UIButtonTypeCustom];
    [_nightMode setFrame:CGRectMake(_arMode.frame.origin.x - 59, 15, 44, 44)];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"NightModeOn"]){
        [_nightMode setImage:[UIImage imageNamed:@"controls_night_mode_on_red"] forState:UIControlStateNormal];
    } else {
        [_nightMode setImage:[UIImage imageNamed:@"controls_night_mode"] forState:UIControlStateNormal];
    }
    [_nightMode addTarget:self action:@selector(nightModePressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_nightMode];
    
    
    _cameraSmall = [[UIImageView alloc]initWithFrame:CGRectMake(_nightMode.frame.origin.x-40, _music.frame.origin.y + _music.frame.size.height + 30, 20, 20)];
    _cameraSmall.image = [UIImage imageNamed:@"camera"];
    [self.view addSubview:_cameraSmall];
    
    _cameraBig = [[UIImageView alloc]initWithFrame:CGRectMake(_camera.frame.origin.x + _camera.frame.size.width+5, _cameraSmall.frame.origin.y - 5, 30, 30)];
    _cameraBig.image = [UIImage imageNamed:@"camera"];
    [self.view addSubview:_cameraBig];
    
    _cameraSlider = [[UISlider alloc]initWithFrame:CGRectMake(_cameraSmall.frame.origin.x + _cameraSmall.frame.size.width + 15, _cameraSmall.frame.origin.y - 5, _cameraBig.frame.origin.x - _cameraSmall.frame.origin.x - _cameraSmall.frame.size.width - 30, 30)];
    [_cameraSlider setThumbTintColor:[UIColor colorWithRed:155.0/255.0 green:155.0/255.0 blue:155.0/255.0 alpha:1.0]];
    [_cameraSlider setMinimumTrackTintColor:[UIColor colorWithRed:155.0/255.0 green:155.0/255.0 blue:155.0/255.0 alpha:1.0]];
    [_cameraSlider setMaximumTrackTintColor:[UIColor colorWithRed:64.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:1.0]];
    [_cameraSlider setMinimumValue: 0.0f];
    [_cameraSlider setMaximumValue: 9.0f];
    _cameraSlider.value = [[NSUserDefaults standardUserDefaults]floatForKey:@"RealCameraValue"]*10;
    [_cameraSlider setContinuous:YES];
    [_cameraSlider addTarget:self action:@selector(changedCameraSlider:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_cameraSlider];
    
    _discoverMode = [UIButton buttonWithType:UIButtonTypeCustom];
    [_discoverMode setFrame:CGRectMake((screenSize.width-50)/2 - 35, _cameraSlider.frame.origin.y + _cameraSlider.frame.size.height + 20, 50, 50)];
    [_discoverMode setImage:[UIImage imageNamed:@"controls_gyro"] forState:UIControlStateNormal];
    [_discoverMode addTarget:self action:@selector(showDiscoverMode) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_discoverMode];
    
    
    _rateApp = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rateApp setFrame:CGRectMake((screenSize.width-50)/2 + 35, _cameraSlider.frame.origin.y + _cameraSlider.frame.size.height + 20, 50, 50)];
    [_rateApp setImage:[UIImage imageNamed:@"controls_rateapp"] forState:UIControlStateNormal];
    [_rateApp setImage:[UIImage imageNamed:@"controls_rateapp_pink"] forState:UIControlStateDisabled];
    [_rateApp addTarget:self action:@selector(infosPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_rateApp];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_bannerView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    [_iconView setFrame:CGRectMake(5, 5, 50, 50)];
    [_headlineLabel setFrame:CGRectMake(65, 5, self.view.frame.size.width - 175, 20)];
    [_subtitleLabel setFrame:CGRectMake(65, 24, self.view.frame.size.width - 175, 35)];
    [_overlayButton setFrame:_bannerView.frame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)changedCameraSlider:(UISlider*)sender{
    sender.value = roundf(sender.value);
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"ARModeOn"]){
        [[NSUserDefaults standardUserDefaults] setFloat: sender.value*1.0/10 forKey: @"RealCameraValue"];
        [[NSUserDefaults standardUserDefaults] setFloat: sender.value*1.0/10 forKey: @"CameraValue"];
    } else {
        [[NSUserDefaults standardUserDefaults] setFloat: sender.value*1.0/10 forKey: @"RealCameraValue"];
    }

    if([self.delegate respondsToSelector:@selector(changedCameraValue:)]) {
        [self.delegate changedCameraValue:sender.value*1.0/10];
    }
    
    [_infoLabel setText: [NSString stringWithFormat:NSLocalizedString(@"Camera Opacity: %.f", nil), sender.value]];
    [_infoLabel setHidden:NO];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(labelAnimation) object:nil];
    [self performSelector:@selector(labelAnimation) withObject:self afterDelay:3.0];
}

- (void)nightModePressed{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"NightModeOn"]){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"NightModeOn"];
        [_nightMode setImage:[UIImage imageNamed:@"controls_night_mode"] forState:UIControlStateNormal];
        [_infoLabel setText: NSLocalizedString(@"Night Mode: Off", nil)];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NightModeOn"];
        [_nightMode setImage:[UIImage imageNamed:@"controls_night_mode_on_red"] forState:UIControlStateNormal];
        [_infoLabel setText: NSLocalizedString(@"Night Mode: On", nil)];
    }
    
    if([self.delegate respondsToSelector:@selector(nightModePressed)]) {
        [self.delegate nightModePressed];
    }
    [_infoLabel setHidden:NO];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(labelAnimation) object:nil];
    [self performSelector:@selector(labelAnimation) withObject:self afterDelay:3.0];
    [self.popupController dismiss];
}

- (void)arModePressed{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"ARModeOn"]){
        [[NSUserDefaults standardUserDefaults] setFloat:0.0 forKey: @"CameraValue"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ARModeOn"];
        [_arMode setImage:[UIImage imageNamed:@"controls_ar_camera"] forState:UIControlStateNormal];
        [_infoLabel setText: NSLocalizedString(@"AR Mode: Off", nil)];
    } else {
        [[NSUserDefaults standardUserDefaults] setFloat:[[NSUserDefaults standardUserDefaults]floatForKey:@"RealCameraValue"] forKey: @"CameraValue"];

        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ARModeOn"];
        [_arMode setImage:[UIImage imageNamed:@"controls_ar_camera_on"] forState:UIControlStateNormal];
        [_infoLabel setText: NSLocalizedString(@"AR Mode: On", nil)];
    }
    [_infoLabel setHidden:NO];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(labelAnimation) object:nil];
    [self performSelector:@selector(labelAnimation) withObject:self afterDelay:3.0];
    [self.popupController dismiss];
}

- (void)cameraPressed{
    UINavigationController *navigationController = (UINavigationController*)self.presentingViewController;
    MainViewController *presentingViewController = (MainViewController*)navigationController.topViewController;
    presentingViewController.takeScreenshot = YES;
    
    [self.popupController dismiss];
    
    if([self.delegate respondsToSelector:@selector(cameraPressed)]) {
        [self.delegate cameraPressed];
    }
    [_infoLabel setHidden:NO];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(labelAnimation) object:nil];
    [self performSelector:@selector(labelAnimation) withObject:self afterDelay:3.0];
}

- (void)musicPressed{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"MusicOn"]){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"MusicOn"];
        [_music setImage:[UIImage imageNamed:@"controls_music"] forState:UIControlStateNormal];
        [_infoLabel setText: NSLocalizedString(@"Music: Off", nil)];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MusicOn"];
        [_music setImage:[UIImage imageNamed:@"controls_music_on"] forState:UIControlStateNormal];
        [_infoLabel setText: NSLocalizedString(@"Music: On", nil)];
    }
    
    if([self.delegate respondsToSelector:@selector(musicPressed)]) {
        [self.delegate musicPressed];
    }
    [_infoLabel setHidden:NO];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(labelAnimation) object:nil];
    [self performSelector:@selector(labelAnimation) withObject:self afterDelay:3.0];
    [self.popupController dismiss];
}

- (void)satellitesPressed{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"SatellitesOn"]){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SatellitesOn"];
        [_satellites setImage:[UIImage imageNamed:@"controls_tracks"] forState:UIControlStateNormal];
        [_infoLabel setText: NSLocalizedString(@"Show Satellites: Off", nil)];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SatellitesOn"];
        [_satellites setImage:[UIImage imageNamed:@"controls_tracks_on"] forState:UIControlStateNormal];
        [_infoLabel setText: NSLocalizedString(@"Show Satellites: On", nil)];
    }
    if([self.delegate respondsToSelector:@selector(satellitesPressed)]) {
        [self.delegate satellitesPressed];
    }
    [_infoLabel setHidden:NO];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(labelAnimation) object:nil];
    [self performSelector:@selector(labelAnimation) withObject:nil afterDelay:3.0];
}

- (void)labelAnimation{
    [_infoLabel setHidden:YES];
}

- (void)infosPressed{
    NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id375380948?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];

    /*UINavigationController *navigationController = (UINavigationController*)self.presentingViewController;
    MainViewController *presentingViewController = (MainViewController*)navigationController.topViewController;
    presentingViewController.showMore = YES;
    
    [self.popupController dismiss];*/
    [self.popupController dismiss];
}

- (void)gyroPressed{
    if([self.delegate respondsToSelector:@selector(gyroPressed)]) {
        [self.delegate gyroPressed];
    }
}

- (void)settingsPressed{
    if([self.delegate respondsToSelector:@selector(settingsPressed)]) {
        [self.delegate settingsPressed];
    }
}

- (void)showDiscoverMode{
    UINavigationController *navigationController = (UINavigationController*)self.presentingViewController;
    MainViewController *presentingViewController = (MainViewController*)navigationController.topViewController;
    presentingViewController.showDiscover = YES;
    
    [self.popupController dismiss];
}

@end
