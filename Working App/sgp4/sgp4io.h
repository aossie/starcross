#ifndef _sgp4io_
#define _sgp4io_


#if _WIN32
# pragma warning(disable: 4018) // signed/unsigned mismatch
# pragma warning(disable: 4244) // conversion possible loss of data
# pragma warning(disable: 4305) // truncation of double to float
# pragma warning(disable: 4996) // CRT_SECURE junk
#endif


#include <stdio.h>
#include <math.h>

#include "sgp4ext.h"    // for several misc routines
#include "sgp4unit.h"   // for sgp4init and getgravconst

// ------------------------- function declarations -------------------------

void twoline2rv
     (
      char      longstr1[130], char longstr2[130],
      char      typerun,  char typeinput, char opsmode,
      gravconsttype       whichconst,
      double& startmfe, double& stopmfe, double& deltamin,
      elsetrec& satrec
     );

#endif

