#ifndef __STARCROSS_STARCROSS_H__
#define __STARCROSS_STARCROSS_H__

#include "rendering.h"
#include "starlist.h"

extern float RampDownTime;

namespace starcross {
    
    enum AppModeEnum {
		AppMode_INVALID,
		AppMode_ViewStars,
		AppMode_ViewGlobe,
		AppMode_MAX
	};
   
	void Display();	
	bool ProcessInput( bool active, int x, int y );
	AppModeEnum GetDisplayMode();
    
	inline float ModuloRange( float f, float lower, float upper ) {
		float delta = upper - lower;
		float fndiff = ( f - lower ) / delta;
		float fnfrac = fndiff - floor( fndiff );
		return fnfrac * delta + lower;
	}
	
}

#endif //__STARCROSS_STARCROSS_H__
