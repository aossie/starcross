#ifndef __STARCROSS_BUTTON_H__
#define __STARCROSS_BUTTON_H__

#include "stars/binding.h"
#include "stars/texture.h"
#include "stars/variable.h"

namespace starcross {
	
	class Button {
	protected:
		r3::Texture2D *tex;		
		Button( const std::string & bTextureFilename );
		bool inputOver;
	public:
		virtual ~Button();
		bool ProcessInput( bool active, int x, int y ); 
		virtual void Draw();
		virtual void Pressed() {}
		r3::Bounds2f bounds;		
		r3::Vec4f color;
	};
	
	class PushButton : public Button {
	protected:
		std::string command;
	public:
		PushButton( const std::string & pbTextureFilename, const std::string & pbCommand );		
		virtual void Pressed();
	};
	
	class ToggleButton : public Button {
	protected:
		r3::Var *var;
	public:
		ToggleButton( const std::string & tbTextureFilename, const std::string & tbVarName );
		virtual void Draw();
		virtual void Pressed();
		r3::Vec4f onColor;
		r3::Vec4f offColor;
	};
	
	
}

#endif // __STARCROSS_BUTTON_H__


