How to Install?

REQUIREMENTS: You must have Xcode 9 or newer version, And an iOS device with atleast iOS 9.

1). Clone or simply download this repository.

2). Install Cocoapods by using "pod install".

3). cd Starcross/working app/iphone/

4). Open starcross.xcworkspace

5). Build and run the app. If you are running in a physical device, you will have to approve certification by; Settings->General->Device Management->Trust.


This app is compatible to all iOS devices with atleast iOS 9.
